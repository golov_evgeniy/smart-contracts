pragma solidity ^0.4.18;

/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {

    /**
    * @dev Multiplies two numbers, throws on overflow.
    */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        assert(c / a == b);
        return c;
    }

    /**
    * @dev Integer division of two numbers, truncating the quotient.
    */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a / b;
        return c;
    }

    /**
    * @dev Substracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b <= a);
        return a - b;
    }

    /**
    * @dev Adds two numbers, throws on overflow.
    */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }
}

/**
* @title ForgePreICO
* @dev Contract to preSale ForgeCDN tokens.
*/
contract ForgePreICO {
    using SafeMath for uint256;

    // The token being sold
    ForgeCDN public token;
    // start and end timestamps where investments are allowed (both inclusive)
    uint256 public startTime;
    uint256 public endTime;
    // address where funds are collected
    address public wallet;
    // how many token units a buyer gets per wei
    uint256 public rate;
    // amount of raised money in wei
    uint256 public weiRaised;
    // Hardcap
    uint256 public cap;

    /**
     * event for token purchase logging
     * @param purchaser who paid for the tokens
     * @param beneficiary who got the tokens
     * @param value weis paid for purchase
     * @param amount amount of tokens purchased
     */
    event TokenPurchase(address indexed purchaser, address indexed beneficiary, uint256 value, uint256 amount);

    function ForgePreICO(address _wallet, ForgeCDN _token) public
    {
        require(_wallet != address(0));
        require(_token != address(0));
        cap = 2400 * 1 ether;
        startTime = 1522670400;
        endTime = 1525003200;
        rate = 1500;
        wallet = _wallet;
        token = _token;
    }

    /**
     * fallback function to buy tokens
     */
    function() external payable {
        address beneficiary = msg.sender;
        require(beneficiary != address(0));
        require(validPurchase());
        uint256 weiAmount = msg.value;

        // calculate token amount to be send
        uint256 tokens = weiAmount.mul(rate);

        // update state
        weiRaised = weiRaised.add(weiAmount);

        token.transfer(beneficiary, tokens);
        TokenPurchase(msg.sender, beneficiary, weiAmount, tokens);

        forwardFunds();
    }

    /**
     * @return true if crowdsale event has ended
     */
    function hasEnded() public view returns (bool) {
        bool capReached = weiRaised >= cap;
        bool timeReached = now > endTime;
        return capReached || timeReached;
    }

    /**
     * send ether to the fund collection wallet
     */
    function forwardFunds() internal {
        wallet.transfer(msg.value);
    }


    /**
     * @return true if the transaction can buy tokens
     */
    function validPurchase() internal view returns (bool) {
        bool withinPeriod = now >= startTime && now <= endTime;
        bool nonZeroPurchase = msg.value != 0;
        bool standardValidation = withinPeriod && nonZeroPurchase;
        bool withinCap = weiRaised.add(msg.value) <= cap;
        return withinCap && standardValidation;
    }

}
